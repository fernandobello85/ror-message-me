# README
This is the Message me app from the Complete Ruby on Rails Developer course.


* Rails version: 5.2.4.3
* Ruby version: 2.6.3 (x86_64-darwin19)
* Semantic-ui-sass 2.4.2


* ActionCable using websocket
