class ChatroomsController < ApplicationController
  before_action :set_chatroom, only: :show
  before_action :require_user, only: :show

  def index
    @chatrooms = Chatroom.paginate(page: params[:page], per_page: 5).order(topic: :asc)

  end

  def show
    @message = Message.new
    @messages = @chatroom.messages.latest_messages
  end

  private

  def set_chatroom
    @chatroom = Chatroom.find(params[:id])
  end
end
