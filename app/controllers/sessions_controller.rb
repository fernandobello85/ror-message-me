class SessionsController < ApplicationController

  before_action :check_if_logged_in?, except: [:destroy]

  def new
  end

  def create
    user = User.find_by(username: params[:session][:username])
    if user&.authenticate(params[:session][:password])
      session[:user_id] = user.id
      flash[:success] = 'Logged in successfully'
      redirect_to root_path
    else
      flash.now[:error] = 'There was something wrong with your login credentials.'
      render 'new'
    end
  end

  def destroy
    session[:user_id] = nil
    flash[:success] = 'Logged out'
    redirect_to login_path
  end

  private

  def check_if_logged_in?
    return unless logged_in?

    flash[:error] = 'You are already logged in.'
    redirect_to root_path
  end
end
