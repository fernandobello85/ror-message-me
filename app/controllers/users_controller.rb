class UsersController < ApplicationController
  before_action :check_if_logged_in?, only: [:new]

  def index
    @users = User.paginate(page: params[:page], per_page: 5).order(created_at: :desc)
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      session[:user_id] = @user.id
      flash[:success] = "Hey #{@user.username}!  Welcome to Message Me."
      redirect_to root_path
    else
      flash.now[:error] = 'There was something wrong while Sign Up.'
      render 'new'
    end
  end

  private

  def user_params
    params.require(:user).permit(:username, :password)
  end

  def check_if_logged_in?
    return unless logged_in?

    flash[:error] = 'You are already logged in.'
    redirect_to root_path
  end
end
